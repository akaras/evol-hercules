// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_LOGIN_LOGIN
#define EVOL_LOGIN_LOGIN

bool elogin_check_password(const char* md5key, int *passwdenc, const char* passwd, const char* refpass);

#endif  // EVOL_LOGIN_SEND
