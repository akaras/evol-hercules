// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_UNIT
#define EVOL_MAP_UNIT

int eunit_can_move(struct block_list *bl);
int eunit_walktoxy(struct block_list *bl, short *x, short *y, int *flagPtr);

#endif  // EVOL_MAP_UNIT
