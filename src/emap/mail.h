// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_MAIL
#define EVOL_MAP_MAIL

bool email_invalid_operation(struct map_session_data *sd);

#endif  // EVOL_MAP_MAIL
