// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_QUEST
#define EVOL_MAP_QUEST

struct quest_db *equest_read_db_sub(struct config_setting_t *cs, int *nPtr, const char *source);

#endif  // EVOL_MAP_QUEST
